
data "aws_eks_cluster" "cluster" {
  name = module.EKScluster.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.EKScluster.cluster_id
}

resource "aws_iam_policy_attachment" "EKS-attach-policy" {
  name       = "EKS-Policy-Attach"
  roles      = ["${module.EKScluster.worker_iam_role_name}"]
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}


provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.9"
}

module "EKScluster" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = "EKScluster"
  cluster_version = "1.16"
  subnets         = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id
  tags = {
    Terraform        = "True"
    Environment      = "dev"
    Application      = "website"
    Owner            = "shj"
    patchEnabled     = "True"
    inventoryEnabled = "True"
  }
  worker_groups = [
    {
      name                = "workergroup-1"
      instance_type       = "t3.small"
      asg_max_size        = 2
      asg_min_size        = 2
      asg_desired_capacity = 2
      additional_userdata = "cd /tmp && sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm && sudo systemctl enable amazon-ssm-agent && sudo systemctl start amazon-ssm-agent"
    }
  ]
}
