
provider "aws" {
  region = "eu-west-1"
}

terraform {
  backend "s3" {
    bucket = "tfstate-shj-phej8cai"
    key    = "tfstate/terraform.tfstate"
    region = "eu-west-1"
  }
}
