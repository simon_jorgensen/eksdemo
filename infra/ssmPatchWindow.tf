resource "aws_ssm_maintenance_window" "ssm-maintenance-window" {
  allow_unassociated_targets = true
  name                       = "EKSPatch"
  schedule                   = "cron(0/30 * * * ? *)"
  duration                   = 1
  cutoff                     = 0
}

resource "aws_ssm_maintenance_window_target" "ssm-maintenance-window-target" {
  resource_type = "INSTANCE"
  targets {
    key = "tag:patchEnabled"
    values = [
      "True"
    ]
  }

  window_id = "${aws_ssm_maintenance_window.ssm-maintenance-window.id}"
}